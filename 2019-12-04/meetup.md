# TYPO3 Meetup

TYPO3 v10 - Was hat sich geändert?

---

# Roadmap v10

https://typo3.org/cms/roadmap/

---

# v10.0

Pave the way for exciting new concepts and APIs & necessary breaking changes

https://forge.typo3.org/attachments/download/34503/TYPO3-v10-0-whats-new.german.pdf

---

# Update von TYPO3 v9 LTS

- Ein Update auf TYPO3 v10 sollte einfach und unkompliziert sein.
- Empfohlen wird die TYPO3 Extension: [Extension Scanner](https://docs.typo3.org/m/typo3/reference-coreapi/master/en-us/ApiOverview/ExtensionScanner/Index.html)
  - (ADMIN TOOLS → Upgrade → Scan Extension Files), um mögliche Probleme zu identifizieren.
  - Typische Probleme sind speziell entwickelte Erweiterungen, die alte APIs verwenden, die gemäß der Deprecation Policy von TYPO3 veraltet oder entfernt wurden. Der **Extension Scanner** bietet nützliche und umfassende Anweisungen zum Aktualisieren des Codes.

---

# System Requirements

- TYPO3 v10 erfordert PHP Version 7.2 oder höher (gleiche PHP-Anforderung wie TYPO3 v9 LTS).
- MySQL (5.7+, empfohlen: 8+), MariaDB (10.2.7+, empfohlen: 10.3), Microsoft SQL Server, PostgreSQL und SQLite.
- Apache, Nginx und IIS.
- Das Backend von TYPO3 v10 ist mit allen modernen Browsern wie Mozilla Firefox, Microsoft Edge und Google Chrome zugänglich. Obwohl es weiterhin möglich ist, mit dem Microsoft Internet Explorer in der Administrationsoberfläche von TYPO3 zu arbeiten, wird er offiziell nicht mehr unterstützt.

---

## Backend User Interface

---

### Dateilistensortierung

- Dateien können jetzt nach ihrem Metadatentitel im Inhaltselement "File Links" sortiert werden.

---

### Link Handler

- Ein neuer Link-Handler wurde hinzugefügt, mit dem Backend-Benutzer Links zu den Telefonnummern einrichten können (mithilfe des `tel:` Protokolls).

Note: Demo - https://ddev-typo3-10-base.ddev.site/typo3/

---

### SEO Sitemap (1)

- `EXT:seo` unterstützt jetzt Änderungshäufigkeiten und -prioritäten für die Sitemap. Die Seiteneigenschaften (Tab "SEO") enthalten zwei neue Felder.

Note: Demo - https://ddev-typo3-10-base.ddev.site/typo3/

---

### SEO Sitemap (2)

- Diese Einstellungen können auch in TypoScript definiert und den Feldern in der Datenbank zugeordnet werden.

  ```
  plugin.tx_seo {
      config {
          xmlSitemap {
              sitemaps {
                  <unique key> {
                      provider = TYPO3\CMS\Seo\XmlSitemap\RecordsXmlSitemapDataProvider
                      config {
                          ...
                          changeFreqField = news_changefreq
                          priorityField = news_priority
                          ...
                      }
                  }
              }
          }
      }
  }
  ```

---

### Formulare

- Beschriftungen von Formularelementen können im Strukturbaum durch Doppelklick auf den Titel bearbeitet werden.

Note: Demo - https://ddev-typo3-10-base.ddev.site/typo3/

---

## Änderungen für Integratoren und Entwickler

### Seite nicht gefunden Handling

- Die folgenden globalen TYPO3-Einstellungen wurden entfernt:

  ```
  $GLOBALS[’TYPO3_CONF_VARS’][’FE’][’pageNotFound_handling’]
  $GLOBALS[’TYPO3_CONF_VARS’][’FE’][’pageNotFound_handling_statheader’]
  $GLOBALS[’TYPO3_CONF_VARS’][’FE’][’pageNotFound_handling_accessdeniedheader’]
  $GLOBALS[’TYPO3_CONF_VARS’][’FE’][’pageUnavailable_handling’]
  $GLOBALS[’TYPO3_CONF_VARS’][’FE’][’pageUnavailable_handling_statheader’]
  ```

  Das in TYPO3 v9 eingeführte Site Handling ersetzt diese Einstellungen.

- Die **"Seite wird generiert"** Nachricht und die entsprechende HTTP 503 Antwort wurden entfernt.

---

### Fluid

- Das Entfernen von Leerzeichen in der `EXT:fluid_styled_content` Datei hat gelegentliche Probleme verursacht und wurde entfernt.

---

### Formular-Framework: Mehrere Empfänger (1)

- Die vom _EmailFinisher_ gesendeten Mails können nun mehrere Empfänger haben.
- Die folgenden neuen Optionen wurden eingeführt:
  - `recipients` (To)
  - `replyToRecipients` (Reply-To)
  - `carbonCopyRecipients` (CC)
  - `blindCarbonCopyRecipients` (BCC)

---

### Formular-Framework: Mehrere Empfänger (2)

- Diese Änderung erfordert eine manuelle Migration von Einzelwertopionen zu ihren Listenwertnachfolgern.
- Die alte Finisher Konfiguration:

  ```
  finishers:
      -
          identifier: EmailToReceiver
          options:
              recipientAddress: user@example.com
              recipientName: ’Firstname Lastname’
  ```

- Die neue Finisher Konfiguration:

  ```
  finishers:
      -
          identifier: EmailToReceiver
          options:
              recipients:
                  user@example.com: ’Firstname Lastname’
  ```

---

### Formular-Framework: Klartext/HTML (1)

- Die vom _EmailFinisher_ gesendeten Mails können nun sowohl Klartext als auch HTML enthalten.
- Die Option `format` wurde gleichzeitig als veraltet markiert und wird in TYPO3 v11 entfernt werden.
- Bestehende Werte werden automatisch angepasst:
  - `format:html` → `addHtmlPart:true`
  - `format:plaintext` → `addHtmlPart:false`
  - fehlendes "`format`" → `addHtmlPart:true`

---

### Frontend Login: Extbase

- Die Benutzeranmeldung im Frontend (`EXT:felogin`) wird in Extbase und Fluid konvertiert.
- Dies ist ein langfristiges Ziel, das aktuell noch [in Arbeit](https://forge.typo3.org/issues/84262) ist.
- Weitere Neuigkeiten in der Version 10.2

---

### ISO Codes

- Das nicht verwendete Datenbankfeld `static_lang_isocode` wurde entfernt.
- `EXT:static_info_tables` kann installiert werden, um die Funktionalität bei Bedarf erneut zu implementieren.
- Entwicklern wird empfohlen, alle Metadaten für eine Sprache mithilfe der Site-Konfiguration und der SiteLanguage-API abzurufen.

---

### Sonstiges

- Die IP-Sperrfunktion wurde erweitert und unterstützt nun auch IPv6
  (Frontend und Backend).

      ```
      $GLOBALS[’TYPO3_CONF_VARS’][’FE’][’lockIPv6’] = 2;
      $GLOBALS[’TYPO3_CONF_VARS’][’BE’][’lockIPv6’] = 2;
      ```

---

## Änderungen für Integratoren

---

### TypoScript (1)

- Die TypoScript-Eigenschaft `config.cache` unterstützt jetzt das Stichwort "`current`" um auf die aktuelle Seite zu verweisen. Zum Beispiel:

  ```
  config.cache.all = fe_users:current
  ```

  https://docs.typo3.org/m/typo3/reference-typoscript/master/en-us/Setup/Config/Index.html#cache

- Die folgenden zwei Optionen zum Konfigurieren der Größe von Popup-Fenstern wurden entfernt:

  ```
  options.popupWindowSize
  options.rte.popupWindowSize
  ```

---

### TypoScript (2)

- Das Datenbankfeld `nextLevel` der Datenbanktabelle `sys_template` wurde entfernt.
- Folgende Werte sind nicht erlaubt:

  ```
  typolink.addQueryString.method = POST
  typolink.addQueryString.method = GET,POST
  typolink.addQueryString.method = POST,GET
  ```

  → Ändere die Zuweisungen in TypoScript, Fluid und PHP.

---

### Caches

- Das Caching-Framework unterstützt das `ApcBackend` nicht mehr. Verwende stattdessen **APCu** beachte das "u":

  ```
  $GLOBALS[’TYPO3_CONF_VARS’][’SYS’][’caching’][’cacheConfigurations’][’rootline’][’backend’] =
  \TYPO3\CMS\Core\Cache\Backend\ApcuBackend::class;
  ```

---

### Formular-Framework

- Die folgende Option wurde umbenannt:

  ```
  translationFile → translationFiles
  ```

- Die benutzerdefinierten YAML-Konfigurationsdateien müssen aktaulisiert werden.

  - Alt:

    ```
    translationFile: path/to/locallang.xlf
    ```

  - Neu:

    ```
    translationFiles:
        20: path/to/locallang.xlf
    ```

---

### Cache Speichertyp

- TYPO3 verfügt über ein flexibles Caching-System mit einer Standardkonfiguration, die für die meisten Anwendungsfälle ideal ist.
- Der Speichertyp kann jetzt konfiguriert werden, um die Caches zu optimieren und die Leistung je nach individueller Umgebung zu steigern.
  - Wähle den **Datenbank** Speicher für eine Standardumgebung oder wenn beispielsweise ein Netzwerkdateisystem (NFS) verwendet wird.
  - Wähle das **Dateisystem** wenn beispielsweise eine verteilte Datenbank eingerichtet wird.
  - Verwende **custom cache settings**, um den Speicher für jeden Cachetyp einzeln zu konfigurieren.
  - Backend: WARTUNG → Einstellungen → Cache

Note: Demo - https://ddev-typo3-10-base.ddev.site/typo3/

---

### EXT:taskcenter und EXT:sys_action

- Die Erweiterungen `EXT:taskcenter` and `EXT:sys_action` wurden aus dem Core entfernt.
- Sie sind jetzt als separate Erweiterungen in der TER oder GitHub verfügbar.
- Behalte die [Dashboard Initiative](https://typo3.org/community/teams/typo3-development/initiatives/typo3-dashboard-initiative/) im Auge, um einen neuen und viel besseren Lösungsansatz zu finden.

---

### Dashboard

https://typo3.org/community/teams/typo3-development/initiatives/typo3-dashboard-initiative/

Note: Demo - https://ddev-typo3-10-base.ddev.site/typo3/

---

### EXT:rsaauth und EXT:fe_edit

- Die bisher enthaltenen System-Erweiterungen `EXT:rsaauth` und `EXT:fe_edit` wurden in das öffentliche Extension-Repository von TYPO3 verschoben.
- Diese Extensions werden auf dem neuesten Stand gehalten, sind aber nicht mehr Teil der Support- und Wartungsrichtlinien von TYPO3, die für den TYPO3-Kern gelten.

---

### Sonstiges (1)

- Der Typ einer Twitter-Karte kann nun ausgewählt/konfiguriert werden. Diese Option gibt das Metatag `twitter:card` im Frontend aus.

  ```
  page {
      meta {
          twitter:card = summary_large_image
          twitter:card.replace = 1
      }
  }
  ```

- In kanonisierten URLs sind standardmäßig nur Parameter enthalten, die zur Berechnung des cHash erforderlich sind. Zusätzliche Abfrageparameter können folgenderweise konfiguriert werden:

  ```
  $GLOBALS[’TYPO3_CONF_VARS’][’FE’][’additionalCanonicalizedUrlParameters’]
  ```

---

### Sonstiges (2)

- Die RTE-Bildbearbeitungsfunktion wurde vollständig entfernt. Verwenden Sie z.B. für die Image-Unterstützung in CKEditor `EXT:rte_ckeditor_image`.
- Beim Rendern von HTML5-Ausgaben, enthalten script-Tags das Attribut `type="text/javascript"` nicht mehr. Dies kann für das Frontend bei Bedarf mit TypoScript wieder aktiviert werden:

  ```
  page {
      includeJS {
          myfile = EXT:example/Resources/Public/JavaScript/myfile.js
          myfile.type = text/javascript
      }
  }
  ```

Note: Demo - https://ddev-typo3-base.ddev.site/typo3/

---

## Änderungen für Entwickler

---

### Neue Mail-API

- TYPO3 hat bisher die SwiftMailer-Bibliothek zum Erzeugen und Versenden von E-Mails genutzt. Die aktive Entwicklung stagniert jedoch und es wurde entschieden, eine weitere Symfony-Lösung mit einer modernen API einzusetzen: das Paket "[Mime](https://symfony.com/doc/current/components/mime.html)" für die Erstellung von E-Mails und das Paket "[Mailer](https://symfony.com/doc/current/components/mailer.html)" für die Verarbeitung und den Versand.
- Beide Komponenten sind auf dem neuesten Stand der Technik und ermöglichen es, HTML-basierte E-Mails an verschiedenen Stellen im Kern zu generieren, an denen bisher nur einfache Text-E-Mails implementiert sind.
  - `symfony/mime` zum Erstellen von E-Mail-Nachrichten
  - `symfony/mailer` zum Versenden von E-Mails
- Die PHP-Function `mail()` wird nicht mehr unterstützt.

  → Es wird empfohlen zum `sendmail` oder `smtp` zu wechseln.

- Weitere Infos zur Nutzung der neuen Mail-API-Funktionen in
  der [Symfony Dokumentation](https://symfony.com/doc/current/mailer.html).

---

## PHP class/property Analyse

- Die Analyse von benutzerdefinierten PHP-Klassen und deren Eigenschaften ist eine Schlüsselfunktion von Extbase. Diese Aufgabe wird nun von der [PropertyInfo-Komponente](https://symfony.com/doc/current/components/property_info.html) von Symfony übernommen (ersetzt die Extbase Reflection Integration).

---

### Symfony Dependency Management/Injection (1)

- Durch die Anwendung der [Service Container-Architektur](https://symfony.com/doc/current/service_container.html) von Symfony wird das Dependency Management und die Dependency Injection für PHP-Klassen auf eine neue Ebene gebracht. Dieser Ansatz zielt darauf ab, den Extbase Dependency Injection Container und den Object Manager zu ersetzen, was bedeutet, dass in Zukunft auf `GeneralUtility::makeInstance()` verzichten werden kann, um Singletons und statische Methoden wie `getInstance()` zu erhalten.
- Das Paket `symfony/dependency-injection` wurde integriert und dient zur Verwaltung des systemweiten Abhängigkeitsmanagements und der Abhängigkeitsinjektion für Klassen.
- Dieser Ansatz zielt darauf ab, den Extbase-Abhängigkeitsinjektionscontainer und den Objektmanager zu ersetzen.

---

### Event Dispatching (1)

Hooks und das Signal/Slot Konzept ist eine der Stärken von TYPO3. Letzteres ermöglicht es, die Kernfunktionalität um ein Signal zu erweitern und andere Komponenten über ein bestimmtes Ereignis zu informieren. TYPO3 Extension-Entwickler können diese Technologie nutzen und auf diesem Core-Feature aufbauen.

- Es wurde [EventDispatcher Component](https://symfony.com/doc/current/components/event_dispatcher.html) zum TYPO3-Kern hinzugefügt (klar spezifiziert in PSR-14), der die gleiche API wie das Zend Framework oder die EventDispatcher-Komponente von Symfony hat. "Events" werden im Laufe der nächsten TYPO3-Sprint Releases hinzugefügt und diese Technologie soll mittelfristig Hooks und Signal/Slots ersetzen.
- Extensionentwickler brauchen sich keine Sorgen zu machen: Hooks und registrierte Slots bleiben so, wie sie jetzt sind und werden vorerst wie bisher funktionieren.

---

### Event Dispatching (2)

- Implementierungsbeispiel - füge der Datei `Configuration/Services.yaml` den Tag `event.listener` hinzu:

  ```
  services:
      Vendor\Example\EventListener\NullMailer:
          tags:
            - { name: event.listener, identifier: ’myListener’, event: TYPO3\CMS\Core\Mail\Event\AfterMailerInitializationEvent, before: ’redirects, anotherIdentifier’ }
  ```

---

### Event Dispatching (3)

- Im Backend kann man auf die Liste der verfügbaren Event Listener zugreifen - erfordert die Systemerweiterung `EXT:lowlevel`

---

### PSR-3 Logging Schnittstelle

- Das Logging Framework von TYPO3 (insbesondere LogLevel und LogManage) verwendet jetzt die [PSR-3 Logger Schnittstelle](https://www.php-fig.org/psr/psr-3/).
- PSR-3 ist eine standardisierte Methode, mit der Bibliotheken ein `Psr\Log\LoggerInterface`-Objekt erhalten können
- Auf diese Weise können Entwickler benutzerdefinierte Protokollfunktionen verwenden und mit anderen Protokollierungssystemen interagieren.

---

### Caching-Framework

- Folgende Caches wurden umbenannt:
  - `cache_core` → `core`
  - `cache_hash` → `hash`
  - `cache_pages` → `pages`
  - `cache_pagesection` → `pagesection`
  - `cache_runtime` → `runtime`
  - `cache_rootline` → `rootline`
  - `cache_imagesizes` → `imagesizes`
- Neue Methode um auf Caches zuzugreifen:

  - Alt:

    ```
    $cacheManager->getCache(’cache_core’).
    ```

  - Neu:

    ```
    $cacheManager->getCache(’core’)
    ```

Das Präfix `cf_` wurde aus den Datenbanktabellen entfernt.

---

### Extbase und Fluid (1)

- Das Registrieren von Plugins/Modulen erfordert nun vollständig qualifizierte Klassennamen

  ```
  \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin()
  \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule()
  ```

- Lasse auch den Vendor im Extension-Namen weg (erstes Argument).

  → Verwende `ExampleBlog` anstelle von `Vendor.ExampleBlog`

  ```
  \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    ’ExampleBlog’, // vorher: ’Vendor.ExampleBlog’
    ’pi1’,
    [
    ...
  ```

---

### Extbase und Fluid (2)

- Es gibt nun in Klassendateien den Modus "strict type" und Type Hints für Skalare.

  ```
  <?php
  declare(strict_types=1);
  ```

- Dies führt zu schwerwiegenden PHP-Fehlern, wenn die Methodensignaturen in benutzerdefinierten Erweiterungen nicht mit den Schnittstellen und/oder übergeordneten Klassen kompatibel sind.

---

### TCA Änderungen

- Das gesamte TCA von `sys_history` wurde entfernt und das Datenbankfeld `pid` wurde gelöscht.
- Der Zugriff auf `$GLOBALS[’TCA’][’sys_history’]` löst eine PHP-Warnung aus.

---

### Dateilisten-Controller

- Die folgenden Controller wurden nach `EXT:filelist` verschoben:

  - `CreateFolderController`
  - `EditFileController`
  - `FileUploadController`
  - `RenameFileController`
  - `ReplaceFileController`

- Infolgedessen wurde der Namespace auch geändert

  ```
  \TYPO3\CMS\Filelist\Controller\File
  ```

---

### Sonstiges

- Die folgenden Datenbankfelder wurden entfernt:
  - `tt_content.spaceBefore` (wird ersetzt durch `space_before_class`)
  - `tt_content.spaceAfter` (wird ersetzt durch `space_after_class`)
  - `pages.t3ver_id`

---

# v10.1

Routing Improvements and Site Handling v2

https://forge.typo3.org/attachments/download/34662/TYPO3-v10-1-whats-new.german.pdf

---

## Backend User Interface

---

### Slug-Updates und Weiterleitungen

- Wenn Backend-Benutzer den URL-Pfad einer Seite ändern (den sogenannten "Slug"), wird die alte URL nicht mehr verfügbar.
- Dies führt möglicherweise zu einem "Seite nicht gefunden" Fehler, auch für die URLs aller Unterseiten.
- Im Rahmen der TYPO3 Version 10.1, wird dies durch zwei Aktionen verhindert:
  - Slugs werden für alle Unterseiten automatisch aktualisiert
  - Es werden Weiterleitungen von den alten zu den neuen URLs erstellt
  - Backend-Benutzer werden über diese Aktionen informiert und können die Änderungen bei Bedarf einfach per Mausklick rückgängig machen

---

## Änderungen für Integratoren

---

### Site-Konfiguration

- Wenn eine neue Seite auf Root Level erstellt wird, wird damit automatisch eine Standard-Site-Konfiguration generiert.
- Demzufolge kann eine grundlegende TYPO3-Site schnell aufgesetzt werden.
- Die Site-Konfigurationsfunktionen:
  - eine vordefinierte Identifier (z.B. `autogenerated-1-c4ca4238a0`)
  - ein Einstiegspunkt (z.B. `https://example.com/autogenerated-1-c4ca4238a0`)
  - eine Default Sprache (z.B. `Englisch`)
- **Site Configuration** ist nun in v10.0 obligatorisch.
  - TYPO3 v9 erlaubt es noch Integratoren `sys_domain` Records (die alte Methode eines Multi-Domain-Setups) zu verwenden.

Note: Demo - https://ddev-typo3-10-base.ddev.site/typo3/

---

### Konflikte bei Redirects (1)

Der Quellpfad kann ein beliebiger Name sein oder er kann als regulärer Ausdruck dargestellt werden. Dies bietet eine großartige Funktionalität, aber was ist, wenn ein Redirect den gleichen Namen wie eine Seiten-URL hat? Solche Konfigurationsfehler können passieren und TYPO3 bietet eine einfache Lösung:

- Ein neuer Symfony-Befehl wurde eingeführt, um Weiterleitungen zu erkennen, die mit Seiten-URLs in Konflikt stehen.
- Führe den Befehl in der CLI aus:

  ```
  $ ./bin/typo3 redirects:checkintegrity
  Redirect (Host: *, Path: /page-1) conflicts with /page-1
  ```

  (Der optionale Parameter `--site` begrenzt die Prüfung auf eine bestimmte Seite)

- Der Befehl ist auch als Scheduler-Task verfügbar

---

### Konflikte bei Redirects (2)

- Im Modul **Berichte** kann auch eine Liste widersprüchlicher Weiterleitungen angezeigt werden
- **Anmerkung:** Der Befehl muss erneut ausgeführt werden, um die Liste _zurückzusetzen_. Durch Beheben des Problems (z.B. durch Entfernen der Weiterleitung) wird die Liste nicht geleert.

---

### Distributionspakete

- Distributionen können jetzt Site-Konfigurationsdateien bereitstellen.
- Erstelle ein Verzeichnis/eine Datei im Distributionspaket wie folgt:

  ```
  Initialisation/Site/<siteIdentifier>/config.yaml
  ```

- Ähnlich wie bei Assets, die nach `fileadmin/` verschoben werden, werden Standardkonfigurationen in den Ordner `config/` verschoben.
- Wenn das Zielverzeichnis bereits vorhanden ist, wird keine Änderung an der vorhandenen Konfiguration vorgenommen.

---

### Vimeo Video-Rendering

- Der Parameter `api=1` in Vimeo-Video-URLs ermöglicht API-Interaktionen mit dem Videoplayer (z.B. Hinzufügen von Schaltächen zur Steuerung des Videos).
- Integratoren können diesen Parameter jetzt auf zwei verschiedene Weisen einstellen:

  - Mit TypoScript:

    - `lib.contentElement.settings.media.additionalConfig.api = 1`
    - In Fluid, mit Media-ViewHelper:

      ```
      <f:media
          file="{file}"
          alt="{file.properties.alternative}"
          title="{file.properties.title}"
          additionalConfig="{api: 1}"
      />
      ```

---

### Tasten für Medienelemente

- Die Tasten **"Add media by URL"** und **"Select & upload files"** können nun unabhängig voneinander aktiviert/deaktiviert werden.
- Im folgenden Beispiel werden beide Tasten ausgeblendet:

  ```
  $GLOBALS[’TCA’][’pages’][’columns’][’media’][’config’][’appearance’] = [
      ’fileUploadAllowed’ => false,
      ’fileByUrlAllowed’ => false,
  ];
  ```

---

## Änderungen für Entwickler

---

### JavaScript Benachrichtigungen

- JavaScript Benachrichtigungen (rechts oben im BE) im BE unterstützen nun Buttons.

---

### PSR-18 HTTP Client

- Die [PSR-18](https://www.php-fig.org/psr/psr-18/) HTTP Client Implementierung wurde hinzugefügt.
- Entwickler können HTTP-Anforderungen basierend auf PSR-7 Nachrichtenobjekten generieren ohne auf eine bestimmte HTTP-Client-Implementierung angewiesen zu sein.
- Dies ersetzt den vorhandenen Wrapper [Guzzle](http://guzzlephp.org/) nicht, sondern bietet eine Alternative.

---

### Benutzerdefinierte Dateiprozessoren

- Entwickler können nun ihre eigenen Dateiprozessoren registrieren.
- Füge diesen Code in die Datei `ext_localconf.php` ein:

  ```
  $GLOBALS[’TYPO3_CONF_VARS’][’SYS’][’fal’][’processors’][’ExampleImageProcessor’] = [
    ’className’ => \Vendor\MyExtension\Resource\Processing\ExampleImageProcessor::class,
    ’before’ => ’LocalImageProcessor’,
  ];
  ```

- Typische Anwendungsfälle:
  - Wasserzeichen auf Bilder setzen
  - hochgeladene Dateien in ein ZIP-Archiv komprimieren
  - bearbeitete Kopien von Bildern speichern
  - ...

---

## Veraltete/Entfernte Funktionen

---

### XML-Sprachdateien

Das XLIFF-Format wird für Sprachdateien seit TYPO3 v4.6 verwendet.

- Die Verwendung von XML-Sprachdateien wird nun als veraltet markiert und löst eine Warnung/einen Fehler aus.

---

## Sonstiges

---

### Erhöhung der Sicherheit

- Externe Links, die von TypoLink generiert wurden, oder Links die `_blank` verwenden, zeigen jetzt das Attribut `rel="noopener noreferrer"`.
- Damit soll die Sicherheit der TYPO3-Website erhöht werden:
  - Der **"noopener"** weist den Browser an, den Link zu öffnen, ohne dem neuen Broswerkontext Zugriff auf das Dokument zu gewähren, das ihn geöffnet hat (Cross Site Hacking).
  - Der **"noreferrer"** verhindert, dass der Browser beim Navigieren zu einer anderen Seite die Seitenadresse oder einen anderen Wert über die Funktion `Referer:` HTTP Header sendet.

---

### YAML-Dateieinbindung

- YAML-Dateien können bereits von anderen YAML-Dateien mit der folgenden Syntax eingebunden werden:

  ```
  imports:
    - { resource: "EXT:my_extension/Configuration/FooBar/Example.yaml" }
  ```

- Dies wurde erweitert, um Ressourcen relativ zur aktuellen YAML-Datei zu importieren:

  ```
  imports:
    - { resource: "subfolder/AnotherExample.yaml" }
    - { resource: "../path/to/configuration/AnotherExample.yaml" }
  ```

---

# v10.2

Fluid / Rendering Engine Improvements

TYPO3 v10.2 unterstützt nicht nur Symfony Version 5.0, sondern ist auch das erste TYPO3-Release, das PHP Version 7.4 unterstützt. Die TYPO3-Entwickler arbeiten aber auch daran, TYPO3 v9 mit PHP 7.4 kompatibel zu machen (natürlich ohne niedrigere Versionen zu brechen).

https://forge.typo3.org/attachments/download/34744/TYPO3-v10-2-whats-new.english.pdf

---

## Backend User Interface

---

### Pagetree Accessibility

- Backend-Benutzer können nun mit der Tastatur durch den Seitenbaum navigieren, zum Beispiel mit den Pfeiltasten, "home", "end", "enter", "space", etc.
  - [WAI-ARIA Authoring Practices 1.1](https://www.w3.org/TR/wai-aria-practices-1.1/#keyboard-interaction-22)

---

## Änderungen für Integratoren

---

### Site Konfiguration (1)

- Der Seitentitel kann nun in **SITE CONFIGURATION → Sites** konfiguriert werden. Auf diese Weise können Integratoren verschiedene Seitentitel pro Sprache angeben.
- Das Feld im Template Datensatz ist obsolet und wurde als **deprecated** markiert.
- Das Feld `sys_template.sitetitle` (Datenbank und TCA) wird in TYPO3 v11 entfernt.
- Der Seitentitel wird sowohl für den Seitentitel als auch für zukünftige Integrationen von `schema.org` verwendet.

Note: Demo - https://ddev-typo3-10-2-base.ddev.site/typo3/

---

### Site Konfiguration (2)

- Es ist nun möglich, Umgebungsvariablen beim Import von YAML-Dateien der Site Konfiguration zu verwenden:

  ```
  imports:
      - resource: ’Env_%env("foo")%.yaml’
  ```

---

### Frontend Login (1)

- TYPO3 v10.2 enthält nun eine Extbase-Version der Frontend-Login-Funktionalität.
- Diese Lösung hat einige Vorteile:
  - Änderungen der Templates werden einfacher.
  - Sende HTML-basierte E-Mails zur Passwort-Wiederherstellung.
  - Passe die Validatoren an und ändere sie, um die Passwortstärke zu erhöhen.
- Bestehende TYPO3-Instanzen werden weiterhin die alten Templates verwenden.
- Integratoren können zwischen dem "alten" und dem "neuen" Plugin wechseln, indem sie den Feature Toggle (BE) verwenden.
- Ab v11 wird es nur die neue Extbase-Variante mit den fluidbasierten Templates geben.

---

### Frontend Login (2)

- Ein Formular zur Wiederherstellung des Passworts wurde hinzugefügt.
- Benutzer können eine Passwortänderung anfordern und erhalten eine E-Mail mit einem Link, der sie zum Formular weiterleitet.
- Standard-Passwortüberprüfungsregeln:
  - `NotEmptyValidator` - Passwörter dürfen nicht leer sein.
  - `StringLengthValidator` - Passwörter müssen eine Mindestlänge haben.

---

### Frontend Login (3)

- Diese Validierungsregeln können angepasst werden, zum Beispiel:

  ```
  plugin.tx_felogin_login {
      settings {
          passwordValidators {
              10 = TYPO3\CMS\Extbase\Validation\Validator\AlphanumericValidator
              20 {
                  className = TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator
                  options {
                      minimum = 12
                      maximum = 32
                  }
              }
              30 = \Vendor\MyExtension\Validation\Validator\MyCustomPasswordPolicyValidator
          }
      }
  }
  ```

---

### Localization Management Platform

- [crowdin](https://crowdin.com/) zielt darauf ab, die bestehende Lösung [Pootle](https://translation.typo3.org/) als Lokalisierungs- und Übersetzungsmanagementplattform zu ersetzen.
- In TYPO3 v10.2 wurde ein Feature Toggle hinzugefügt, der crowdin.com als Quelle für Übersetzungen verwendet, wenn er aktiviert ist.
- Bitte beachte: Diese Umstellung ist noch im **beta-Status**.
- Lese mehr über die [Initiative](https://typo3.org/community/teams/typo3-development/initiatives/localization-with-crowdin/).

Note: https://crowdin.com/project/typo3-cms

---

### Multiple Sitemaps

- Es ist nun möglich, mehrere Sitemaps zu konfigurieren. Syntax:

  ```
  plugin.tx_seo {
      config {
          <sitemapType> {
              sitemaps {
                  <unique key> {
                      provider = TYPO3\CMS\Seo\XmlSitemap\RecordsXmlSitemapDataProvider
                      config {
                          ...
                      }
                  }
              }
          }
      }
  }
  ```

---

### Link Validator

- Der Link Validator unterstützt nun eine zusätzliche Konfiguration für externe Links.
- Der Link Validator markiert nun auch fehlerhafte externe Links im RTE.
  - Diese Funktion war bisher nur für interne Links verfügbar.
- Es wird empfohlen, den Link Validator als Scheduler-Task auszuführen, um regelmäßig nach fehlerhaften Links zu suchen.

Note: https://docs.typo3.org/c/typo3/cms-linkvalidator/master/en-us/Introduction/Index.html

---

## Änderungen für Entwickler

---

### Widget ViewHelpers

- Widget ViewHelpers setzen unter Umständen ein Session-Cookie im Frontend.
- Da dies nicht immer erwünscht ist (z.B. aufgrund von DSGVO), kann dies nun kontrolliert werden.
- Hierfür wurde storeSession (boolean) eingeführt, darüber kann die Session-Cookie-Funktion aktiviert/deaktiviert werden.

  ```
  <f:widget.autocomplete
      for="name"
      objects="{posts}"
      searchProperty="author"
      storeSession="false" />
  ```

---

### PSR-14 Events in FAL

- Ungefähr 40 neue [PSR-14](https://www.php-fig.org/psr/psr-14/) basierte Ereignisse wurden im File Abstraction Layer (FAL) eingeführt.
- Sie ersetzen bestehende Extbase Signal/Slots.
- Die Verwendung von Signals funktioniert weiterhin (ohne eine Deprecation Warnung!). Allerdings werden Signals im FAL in TYPO3 v11 wahrscheinlich entfernt.
- Autoren von Erweiterungen wird empfohlen, ihren Code zu migrieren und Events zu verwenden.
- Einige Beispiele für die neuen PSR-14 Events:
  - `TYPO3\CMS\Core\Imaging\Event\ModifyIconForResourcePropertiesEvent`
  - `TYPO3\CMS\Core\DataHandling\Event\IsTableExcludedFromReferenceIndexEvent`
  - `TYPO3\CMS\Core\DataHandling\Event\AppendLinkHandlerElementsEvent`
  - `TYPO3\CMS\Core\Configuration\Event\AfterTcaCompilationEvent`
  - `TYPO3\CMS\Core\Database\Event\AlterTableDefinitionStatementsEvent`
  - `TYPO3\CMS\Core\Tree\Event\ModifyTreeDataEvent`
  - `TYPO3\CMS\Backend\Backend\Event\SystemInformationToolbarCollectorEvent`

---

### Prepared Statements

- Es wurden zwei neue PHP-Klassen zum Laden und Parsen von PageTSconfig eingeführt:
  - `TYPO3\CMS\Core\Configuration\Loader\PageTsConfigLoader`
  - `TYPO3\CMS\Core\Configuration\Parser\PageTsConfigParser`
- Beispiel:

  ```
  // Fetch all available PageTS of a page/rootline:
  $loader = GeneralUtility::makeInstance(PageTsConfigLoader::class);
  $tsConfigString = $loader->load($rootLine);

  // Parse the string and apply conditions:
  $parser = GeneralUtility::makeInstance(
      PageTsConfigParser::class, $typoScriptParser, $hashCache
  );

  $pagesTSconfig = $parser->parse($tsConfigString, $conditionMatcher);
  ```

---

### Lazy Loading Proxy

- Eine Methode `getUid()` wurde der Klasse `TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy` hinzugefügt.
- Dies ermöglicht es Entwicklern, die UID des Objekts zu bekommen, ohne das Objekt von der Datenbank abrufen zu müssen.

---

### Native List Pagination

- Mit der neuen Pagination API erhalten wir eine native Unterstützung für die Seitennavigation von Listen wie Arrays oder QueryResults (Extbase).
- Das `PaginatorInterface` definiert einen grundlegenden Satz von Methoden.
- Die Klasse `AbstractPaginator` enthält die Hauptseitenlogik.
- Dies ermöglicht es Entwicklern, alle Arten von Paginatoren zu implementieren.

  ```
  use TYPO3\CMS\Core\Pagination\ArrayPaginator;

  $items = [’apple’, ’banana’, ’strawberry’, ’raspberry’, ’ananas’];
  $currentPageNumber = 3;
  $itemsPerPage = 2;

  $paginator = new ArrayPaginator($itemsToBePaginated, $currentPageNumber, $itemsPerPage);
  $paginator->getNumberOfPages(); // returns 3
  $paginator->getCurrentPageNumber(); // returns 3
  $paginator->getKeyOfFirstPaginatedItem(); // returns 5
  $paginator->getKeyOfLastPaginatedItem(); // returns 5
  ```

---

## System Extension "Form"

Diese Änderungen betreffen sowohl Redakteure als auch Integratoren und Entwickler. Backend-Benutzer profitieren von einem erweiterten Formularerstellungsassistenten, der die Navigation zu vorherigen Schritten und beschreibende Bezeichnungen wie "Anfang" oder "Ende" anstelle der numerischen Anzeige "Schritt x von y" unterstützt.

Integratoren profitieren von einer optimierten Einrichtung (es wird nur eine allgemeine Konfigurationsdatei "`FormSetup.yaml`" verwendet) und einer optimierten Konfigurationsstruktur.

---

### Form Setup

- Bisher wurden drei Dateien verwendet: `BaseSetup.yaml`, `FormEditorSetup.yaml` und `FormEngineSetup.yaml`.
- Das wird nun in einer Datei zusammengefasst: `FormSetup.yaml`.
- Dieses Datei enthält die Grundkonfiguration einschließlich des Imports der Konguration für Validatoren, Form-Elemente und Finisher.
- Alle bisher verwendeten Vererbungen und Mixins wurden aufgelöst, was es sehr einfach macht, die gesamte Konguration zu verstehen.

---

### Multi-step Wizard

- Ein neues JavaScript-Modul `MultiStepWizard` wurde eingeführt, das die folgenden Funktionen bietet:
  - Navigation zu den vorherigen Schritten.
  - Schritte unterstützen beschreibende Bezeichnungen wie "Anfang" oder "Ende" und nicht die numerische Anzeige "Schritt x von y".
  - Optimierte Konfigurationsstruktur.

Note: Demo - https://ddev-typo3-10-2-base.ddev.site/typo3/

---

### RenderFormValue

- Mit dem neuen **RenderFormValue**-ViewHelper können Integratoren/Entwickler auf einzelne Formularwerte in Templates zugreifen:

  ```
  <p>
      The following message was just sent by
      <formvh:renderFormValue renderable="{page.rootForm.elements.name}" as="formValue">
          {formValue.processedValue}
      </formvh:renderFormValue>:
  </p>

  <blockquote>
      <formvh:renderFormValue renderable="{page.rootForm.elements.message}" as="formValue">
          {formValue.processedValue}
      </formvh:renderFormValue>
  </blockquote>
  ```

---

### Fieldset Labels

- Das Abschnittselement `Fieldset` ist nun in Templates verfügbar.
- Standardmäßig betrifft dies sowohl das **SummaryPage** Formular-Element als auch die Finisher **EmailToReceiver** und **EmailToSender**.
- Typischer Anwendungsfall:

  Ein Formular mit einer Versand- und einer Rechnungsadresse. Beide Abschnitte könnten ein Feld mit dem gleichen Namen haben, z.B. `street`. Es ist nun möglich, zwischen beiden Feldern zu unterscheiden, indem man fieldset-Labels verwendet.
